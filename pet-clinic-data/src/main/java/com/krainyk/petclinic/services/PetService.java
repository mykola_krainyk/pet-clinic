package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
