package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long>{

}
