package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.Speciality;

public interface SpecialityService extends CrudService<Speciality, Long> {
}
