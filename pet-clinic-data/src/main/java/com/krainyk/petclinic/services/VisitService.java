package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
