package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {
}
