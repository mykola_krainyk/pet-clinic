package com.krainyk.petclinic.services;

import com.krainyk.petclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {

    Owner findByLastName(String lastName);
}
